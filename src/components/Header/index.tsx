import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';
import HeaderLogo from './HeaderLogo';
import {ITheme} from '../../theme/types';
import {breakpointMixin, flexLayoutMixin} from '../../theme/styleMixins';
import ToggleThemeButton from '../ToggleThemeButton';
import {WindowWidthRestrictionsEnum} from '../../store/reducers/deviceInfoSlice/types';
import {useAppSelector} from '../../store/hooks';

const useStyles = createUseStyles((theme: ITheme) => ({
  header: {
    ...flexLayoutMixin('center', 'space-between'),
    padding: [theme.spacing(2), theme.spacing(1)],
    width: '100%',

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      marginBottom: theme.spacing(2),
      padding: theme.spacing(2),
    }),
  },
}));

const Header: FC = (): ReactElement => {
  const classes = useStyles();
  const {window: deviceInfoWindow} = useAppSelector((state) => state.deviceInfoSlice);

  return (
    <header className={classes.header}>
      <HeaderLogo />

      {
        deviceInfoWindow.width <= WindowWidthRestrictionsEnum.TABLET && <ToggleThemeButton />
      }
    </header>
  );
};

export default Header;
