import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';
import {useHistory} from 'react-router-dom';
import {ITheme} from '../../theme/types';
import {flexLayoutMixin, sizeMixin} from '../../theme/styleMixins';
import {ReactComponent as IconLogo} from './assets/iconLogo.svg';

const useStyles = createUseStyles((theme: ITheme) => ({
  logo: {
    ...flexLayoutMixin(),
    marginRight: theme.spacing(2),
    cursor: 'pointer',
    color: theme.palette.textPrimary,
  },
  logoIcon: {
    ...sizeMixin(55),
    fill: theme.palette.primary,
  },
}));

const HeaderLogo: FC = (): ReactElement => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <div onClick={() => history.push('/')} className={classes.logo}>
      <IconLogo className={classes.logoIcon}/>
    </div>
  );
};

export default HeaderLogo;
