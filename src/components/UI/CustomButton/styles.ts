import {createUseStyles} from 'react-jss';
import {fontWeightEnum, ITheme} from '../../../theme/types';
import {flexLayoutMixin, sizeMixin} from '../../../theme/styleMixins';

export const useStyles = createUseStyles((theme: ITheme) => {
  const buttonHeight= 60;

  return {
    customButton: {
      ...flexLayoutMixin(),
      height: buttonHeight,
      position: 'relative',
      backgroundColor: 'transparent',
      border: 'none',
      padding: [[0, theme.spacing(6)]],
      fontSize: theme.fontSize(1.15),
      fontWeight: theme.fontWeight(fontWeightEnum.BOLD),
      textTransform: 'uppercase',
      color: theme.palette.textPrimary,
      '&::before': {
        ...sizeMixin(buttonHeight),
        content: '""',
        position: 'absolute',
        top: '50%',
        transform: 'translateY(-50%)',
        left: 0,
        borderRadius: buttonHeight / 2,
        backgroundColor: theme.palette.buttonBackground,
        transition: 'all .25s linear',
      },
      '&:hover': {
        cursor: 'pointer',
        '&::before': {
          width: '100%',
          backgroundColor: theme.palette.buttonHover,
        }
      }
    },
    customButtonText: {
      position: 'relative',
      zIndex: 2,
    },
    'button--filled': {

    },
    'button--text': {

    },
    'button--outlined': {

    },
  };
});
