import React, {FC, ReactElement} from 'react';
import classnames from 'classnames';

import {ICustomButtonProps} from './types';
import {useStyles} from './styles';

const CustomButton: FC<ICustomButtonProps> = (props: ICustomButtonProps): ReactElement => {
  const classes = useStyles();
  const {handler, mode, classes: propsClasses, children, ...rest} = props;

  return (
    <button
      onClick={handler}
      className={classnames([classes.customButton, `button--${mode || 'default'}`, ...propsClasses || []])}
      {...rest}>
      <span className={classes.customButtonText}>
        {children}
      </span>
    </button>
  );
};

export default CustomButton;
