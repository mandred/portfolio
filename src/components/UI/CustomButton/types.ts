import React, {ButtonHTMLAttributes, ReactNode} from 'react';

export interface ICustomButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  children?: ReactNode | string,
  mode?: customButtonMode,
  icon?: string,
  loading?: boolean,
  classes?: string[],
  handler?: (e: React.MouseEvent) => void,
}

type customButtonMode = 'filled' | 'outlined' | 'text';
