import {TextareaHTMLAttributes} from 'react';

export interface ICustomTextFieldTextAreaProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  classList?: string[],
}
