import React, {FC, ReactElement} from 'react';
import classnames from 'classnames';
import {ICustomTextFieldTextAreaProps} from './types';
import {useStyles} from './styles';

const CustomTextFieldTextArea: FC<ICustomTextFieldTextAreaProps> = (props: ICustomTextFieldTextAreaProps): ReactElement => {
  const classes = useStyles();

  const {classList: propsClassList, ...rest} = props;

  return (
    <textarea
      className={classnames([classes.customTextFieldTextarea, ...propsClassList || []])}
      {...rest} />
  );
};

export default CustomTextFieldTextArea;
