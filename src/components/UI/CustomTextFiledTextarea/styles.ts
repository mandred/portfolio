import {createUseStyles} from 'react-jss';
import {ITheme} from '../../../theme/types';
import {customScrollBarMixin} from '../../../theme/styleMixins';

export const useStyles = createUseStyles((theme: ITheme) => ({
  customTextFieldTextarea: {
    ...customScrollBarMixin(theme.palette.textPrimary),
    width: '100%',
    maxWidth: '100%',
    maxHeight: 250,
    minWidth: 300,
    minHeight: 100,
    padding: [theme.spacing(1), theme.spacing(2)],
    borderRadius: theme.spacing(1),
    border: `1px solid ${theme.palette.iconPrimary}`,
    fontSize: theme.fontSize(1.1),
    color: theme.palette.textPrimary,
    backgroundColor: 'transparent',
    transition: 'border .3s linear',
    '&:hover': {
      border: `1px solid ${theme.palette.primary}`,
    },
    '&:focus': {
      backgroundColor: theme.palette.background,
      border: `1px solid ${theme.palette.primary}`,
    },
  },
}));
