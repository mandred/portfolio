import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';
import {ReactComponent as IconBallsPreloader} from './assets/iconBallsPrelaoder.svg';
import {ReactComponent as IconCirclesPreloader} from './assets/iconCirclesPreloader.svg';
import {ITheme} from '../../../theme/types';
import {flexLayoutMixin} from '../../../theme/styleMixins';

const useStyles = createUseStyles((theme: ITheme) => ({
  preloaderWrapper: {
    ...flexLayoutMixin('center', 'center'),
    height: '100%',
    width: '8vh',
  },
  preloader: {
    fill: theme.palette.textPrimary,
  },
}));

const CustomPreloader: FC<ICustomPreloaderProps> = (props: ICustomPreloaderProps): ReactElement => {
  const classes = useStyles();
  const {type} = props;

  const CurrentPreloaderComponent = {
    balls : IconBallsPreloader,
    circles: IconCirclesPreloader,
  }[type];

  return (
    <section className={classes.preloaderWrapper}>
      <CurrentPreloaderComponent className={classes.preloader}/>
    </section>
  );
};

export default CustomPreloader;

interface ICustomPreloaderProps {
  type: 'balls' | 'circles',
}
