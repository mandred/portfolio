import {InputHTMLAttributes, ReactNode} from 'react';

export interface ICustomTextFieldInputProps extends InputHTMLAttributes<HTMLInputElement> {
  firstIcon?: ReactNode,
  lastIcon?: ReactNode,
  classList?: string[],
}
