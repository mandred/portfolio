import React, {FC, ReactElement} from 'react';
import classnames from 'classnames';
import {ICustomTextFieldInputProps} from './types';
import {useStyles} from './styles';

const CustomTextFieldInput: FC<ICustomTextFieldInputProps> = (props: ICustomTextFieldInputProps): ReactElement => {
  const classes = useStyles();

  const {classList: propsClassList, ...rest} = props;

  return (
    <input
      className={classnames([classes.customTextFieldInput, ...propsClassList || []])}
      {...rest}/>
  );
};

export default CustomTextFieldInput;
