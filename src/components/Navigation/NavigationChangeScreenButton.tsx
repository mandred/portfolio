import React, {FC, ReactElement, useMemo} from 'react';
import {createUseStyles} from 'react-jss';
import {useHistory, useLocation} from 'react-router-dom';

import {ROUTES} from '../../Router/routes';
import {ITheme} from '../../theme/types';
import {betweenChildrenMixin, flexLayoutMixin, sizeMixin} from '../../theme/styleMixins';
import {ReactComponent as IconArrowBack} from './assets/iconArrowBack.svg';
import {ReactComponent as IconArrowNext} from './assets/iconArrowNext.svg';

const useStyles = createUseStyles((theme: ITheme) => ({
    changeScreenButtonsWrapper: {
        ...flexLayoutMixin('flex-end', 'center'),
        ...betweenChildrenMixin({
            marginBottom: theme.spacing(2),
        }),
        flexFlow: 'column nowrap',
    },
    changeScreenButton: {
        ...flexLayoutMixin(),
        ...sizeMixin(60),
        border: 'none',
        borderRadius: '50%',
        backgroundColor: theme.palette.buttonBackground,
        transition: 'all .25s linear',
        '&:hover': {
            cursor: 'pointer',
            '& $changeScreenButtonIcon': {
                fill: theme.palette.primary,
            },
        },
    },
    changeScreenButtonIcon: {
        ...flexLayoutMixin(),
        ...sizeMixin(55),
        fill: theme.palette.iconPrimary,
        transition: 'all .25s linear',
    },
}));

const NavigationChangeScreenButton: FC = (): ReactElement => {
    const classes = useStyles();
    const {pathname} = useLocation();
    const history = useHistory();

    const routeIdx: number = ROUTES.findIndex((item) => item.pathname === pathname);
    const getPreviousRoute = useMemo(() => routeIdx !== -1 ? ROUTES[routeIdx - 1]?.pathname : '', [pathname]);
    const getNextRoute = useMemo(() => routeIdx !== -1 ? ROUTES[routeIdx + 1]?.pathname : '', [pathname]);

    const changeRoute = (route: string): void => {
        history.push(route);
    };

    return (
        <section className={classes.changeScreenButtonsWrapper}>
            {ROUTES[0].pathname !== pathname &&
              <button
                onClick={() => changeRoute(getPreviousRoute)}
                title={'Back'}
                className={classes.changeScreenButton}>
                <IconArrowBack className={classes.changeScreenButtonIcon}/>
              </button>
            }

            {ROUTES[ROUTES.length - 1].pathname !== pathname &&
              <button
                onClick={() => changeRoute(getNextRoute)}
                title={'Next'}
                className={classes.changeScreenButton}>
                <IconArrowNext className={classes.changeScreenButtonIcon}/>
              </button>
            }
        </section>
    );
};

export default NavigationChangeScreenButton;
