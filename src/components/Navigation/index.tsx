import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';

import NavigationButton from './NavigationButton';
import ToggleThemeButton from '../ToggleThemeButton';
import NavigationChangeScreenButton from './NavigationChangeScreenButton';
import {ITheme} from '../../theme/types';
import {betweenChildrenMixin, breakpointMixin, flexLayoutMixin} from '../../theme/styleMixins';
import {ROUTES} from '../../Router/routes';
import {useAppSelector} from '../../store/hooks';
import {WindowWidthRestrictionsEnum} from '../../store/reducers/deviceInfoSlice/types';

const buttonHeight = 60;

const useStyles = createUseStyles((theme: ITheme) => ({
  navigationWrapper: {
    ...flexLayoutMixin('center', 'space-between'),
    flexFlow: 'column nowrap',
    position: 'relative',
    padding: [theme.spacing(2), theme.spacing(1)],
    height: 'auto',
    width: '100%',
    boxShadow: `0 0 ${theme.spacing(6)}px ${theme.palette.buttonBackground}`,

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      padding: theme.spacing(2),
      height: '100%',
      width: 'auto',
      boxShadow: 'none',
    }),
  },

  navigation: {
    ...flexLayoutMixin('center', 'space-between'),
    ...betweenChildrenMixin({
      marginRight: theme.spacing(1),
    }),
    flexFlow: 'row nowrap',
    width: '100%',

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      ...flexLayoutMixin('flex-end', 'center'),
      ...betweenChildrenMixin({
        marginBottom: theme.spacing(2),
        marginRight: '0px !important',
      }),
      flexFlow: 'column nowrap',
      position: 'absolute',
      top: '50%',
      transform: 'translateY(-50%)',
      width: buttonHeight,
    }),
  },
}));

const Navigation: FC = (): ReactElement => {
  const classes = useStyles();
  const {window: deviceInfoWindow} = useAppSelector((state) => state.deviceInfoSlice);

  return (
    <section className={classes.navigationWrapper}>
      {
        deviceInfoWindow.width > WindowWidthRestrictionsEnum.TABLET && <ToggleThemeButton />
      }

      <div className={classes.navigation}>
        {ROUTES
          .map((item) => (
            <NavigationButton pathname={item.pathname} exact key={item.name}>
              {item}
            </NavigationButton>
          ))}
      </div>

      {
        deviceInfoWindow.width > WindowWidthRestrictionsEnum.TABLET && <NavigationChangeScreenButton/>
      }
    </section>
  );
};

export default Navigation;
