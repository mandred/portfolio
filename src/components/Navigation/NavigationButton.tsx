import React, {FC, ReactElement, SVGProps} from 'react';
import {createUseStyles} from 'react-jss';
import {useHistory, useRouteMatch} from 'react-router-dom';
import classnames from 'classnames';
import {ITheme} from '../../theme/types';
import {IRoute} from '../../Router/routes';
import Home from '../../screens/Home';
import {betweenChildrenMixin, breakpointMixin, flexLayoutMixin, sizeMixin} from '../../theme/styleMixins';
import {ReactComponent as IconHome} from './assets/iconHome.svg';
import {ReactComponent as IconAbout} from './assets/iconAbout.svg';
import {ReactComponent as IconProjects} from './assets/iconProjects.svg';
import {ReactComponent as IconSkills} from './assets/iconSkills.svg';
import {ReactComponent as IconContacts} from './assets/iconContacts.svg';
import {WindowWidthRestrictionsEnum} from '../../store/reducers/deviceInfoSlice/types';

const buttonHeight = 60;

const useStyles = createUseStyles((theme: ITheme) => ({
  navigationButton: {
    ...flexLayoutMixin(),
    ...betweenChildrenMixin({
      marginRight: theme.spacing(1),
    }),
    ...sizeMixin(buttonHeight),
    padding: [[0, theme.spacing(2)]],
    borderRadius: '50%',
    border: 'none',
    backgroundColor: theme.palette.buttonBackground,
    transition: 'all .25s linear',
    '&:hover, &:focus': {
      cursor: 'pointer',
      backgroundColor: theme.palette.primary,
      '& svg': {
        fill: theme.palette.iconSecondary,
      },
    },

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      height: buttonHeight,
      width: 'auto',
      borderRadius: buttonHeight / 2,
      '&:hover, &:focus': {
        cursor: 'pointer',
        backgroundColor: theme.palette.primary,
        '& span': {
          color: theme.palette.textSecondary,
          display: 'block',
        },
        '& svg': {
          fill: theme.palette.iconSecondary,
        },
      },
    }),
  },

  navigationButtonActive: {
    backgroundColor: theme.palette.primary,
  },

  navigationButtonText: {
    textTransform: 'uppercase',
    display: 'none',
    fontSize: theme.fontSize(1.15),
  },

  navigationButtonTextActive: {
    color: theme.palette.textSecondary,
  },

  navigationButtonIcon: {
    fill: theme.palette.iconPrimary ,
    ...sizeMixin(buttonHeight / 2),
  },

  navigationButtonIconActive: {
    fill: theme.palette.iconSecondary,
  },
}));

const NavigationButton: FC<INavigationButtonProps> = (props: INavigationButtonProps): ReactElement => {
  const classes = useStyles();
  const {children, pathname, exact} = props;
  const history = useHistory();
  const match = useRouteMatch({path: pathname, exact});

  const CurrentIcon: FC<SVGProps<SVGSVGElement> & {title?: string | undefined}> | undefined = {
    Home: IconHome,
    About: IconAbout,
    Projects: IconProjects,
    Skills: IconSkills,
    Contacts: IconContacts,
  }[children.name] || Home;

  return (
    <button
      onClick={() => history.push(children.pathname)}
      title={children.name}
      className={classnames([classes.navigationButton, match ? classes.navigationButtonActive : ''])}>
      <span className={classnames([classes.navigationButtonText, match ? classes.navigationButtonTextActive : ''])}>
        {children.name}
      </span>

      <CurrentIcon className={classnames([classes.navigationButtonIcon, match ? classes.navigationButtonIconActive : ''])} />
    </button>
  );
};

export default NavigationButton;

interface INavigationButtonProps {
  children: IRoute,
  pathname: string,
  exact: boolean,
}
