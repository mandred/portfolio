import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';
import {ITheme, themeTypeEnum} from '../theme/types';
import {useAppDispatch, useAppSelector} from '../store/hooks';
import {toggleTheme} from '../store/actions/themeActions';
import {flexLayoutMixin, sizeMixin} from '../theme/styleMixins';
import { ReactComponent as IconLightMode} from './Header/assets/iconLightMode.svg';
import { ReactComponent as IconDarkMode} from './Header/assets/iconDarkMode.svg';

const useStyles = createUseStyles((theme: ITheme) => ({
  themeButton: {
    ...flexLayoutMixin(),
    ...sizeMixin(60),
    border: 'none',
    borderRadius: '50%',
    backgroundColor: theme.palette.buttonBackground,
    transition: 'all .25s linear',
    '&:hover': {
      cursor: 'pointer',
    },
  },
  themeButtonIcon: {
    ...flexLayoutMixin(),
    ...sizeMixin(30),
    fill: theme.palette.iconThemeButton,
  },
}));

const ToggleThemeButton: FC = (): ReactElement => {
  const classes = useStyles();
  const {theme} = useAppSelector((state) => state.themeSlice);
  const dispatch = useAppDispatch();

  return (
    <button
      className={classes.themeButton}
      onClick={() => dispatch(toggleTheme())}
      title={theme === themeTypeEnum.LIGHT ? 'Set dark theme' : 'Set light theme'}>
      {
        theme === themeTypeEnum.LIGHT
          ? <IconDarkMode className={classes.themeButtonIcon} />
          : <IconLightMode className={classes.themeButtonIcon} />
      }
    </button>
  );
};

export default ToggleThemeButton;
