import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';
import {breakpointMixin, flexLayoutMixin} from '../theme/styleMixins';
import {fontWeightEnum, ITheme} from '../theme/types';
import {WindowWidthRestrictionsEnum} from '../store/reducers/deviceInfoSlice/types';

const useStyles = createUseStyles((theme: ITheme) => ({
  screenTitleWrapper: {
    ...flexLayoutMixin(),
    width: '100%',
    padding: [theme.spacing(3), 0],
    position: 'relative',

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      padding: [theme.spacing(4), 0],
    }),
  },
  screenTitle: {
    textTransform: 'uppercase',
    letterSpacing: 10,
    fontSize: theme.fontSize(3),
    fontWeight: theme.fontWeight(fontWeightEnum.EXTRA_BOLD),
    color: theme.palette.primary,
    position: 'relative',
    zIndex: 2,
    '& span': {
      color: theme.palette.primary,
    },
  },
  screenTitleBackground: {
    ...flexLayoutMixin(),
    position: 'absolute',
    left: 0,
    right: 0,
    top: '50%',
    transform: 'translateY(-50%)',
    textTransform: 'uppercase',
    color: theme.palette.buttonBackground,
    opacity: '0.45',
    lineHeight: '0.7',
    letterSpacing: 20,
    fontSize: theme.fontSize(6),
    fontWeight: theme.fontWeight(fontWeightEnum.EXTRA_BOLD),
    zIndex: 1,
  }
}));

const ScreenTitle: FC<IScreenTitleProps> = (props: IScreenTitleProps): ReactElement => {
  const classes = useStyles();
  const {title} = props;

  return (
    <section className={classes.screenTitleWrapper}>
      <h1 className={classes.screenTitle}>
        {title}
      </h1>

      <span className={classes.screenTitleBackground}>
        {title}
      </span>
    </section>
  );
};

export default ScreenTitle;

interface IScreenTitleProps {
  title: string,
}
