import React, {FC, ReactElement, useEffect} from 'react';
import {createUseStyles} from 'react-jss';
import {fetchProjects} from '../../store/actions/projectsActions';
import {useAppDispatch, useAppSelector} from '../../store/hooks';
import ScreenTitle from '../../components/ScreenTitle';
import ProjectsCard from './ProjectsCard';
import {customScrollBarMixin, flexLayoutMixin, sizeMixin} from '../../theme/styleMixins';
import CustomPreloader from '../../components/UI/CustomPreloader';
import {ITheme} from '../../theme/types';

const useStyles = createUseStyles((theme: ITheme) => ({
  projectsWrapper: {
    ...sizeMixin('100%'),
    ...flexLayoutMixin(),
    flexFlow: 'column nowrap',
    overflow: 'hidden',
  },
  projectList: {
    ...customScrollBarMixin(theme.palette.textPrimary),
    ...flexLayoutMixin('flex-start', 'flex-start'),
    ...sizeMixin('100%'),
    flexFlow: 'row wrap',
    overflowY: 'auto',
    padding: [0, theme.spacing(1)],
  },
}));

const Projects: FC = (): ReactElement => {
  const classes = useStyles();
  const {projects, pending} = useAppSelector((state) => state.projectsSlice);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!projects.length) {
      dispatch(fetchProjects());
    }
  }, []);

  return (
    <>
      <ScreenTitle title={'Projects'}/>

      {
        pending
          ? <CustomPreloader type={'balls'}/>
          : <section className={classes.projectsWrapper}>
            {/*<ProjectsNavigation />*/}

            <div className={classes.projectList}>
              {
                projects
                .filter((item) => item.isVisible)
                .map((item) => (
                  <ProjectsCard
                  key={item.id}
                  project={item}/>
                ))
              }
            </div>
        </section>
      }
    </>
  );
};

export default Projects;
