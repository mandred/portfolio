import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';
import {ITheme} from '../../theme/types';
import {flexLayoutMixin} from '../../theme/styleMixins';

const useStyles = createUseStyles((theme: ITheme) => ({
  navWrapper: {
    ...flexLayoutMixin('center', 'space-between'),
    flexFlow: 'row nowrap',
    width: '100%',
    padding: [theme.spacing(2), 0],
  },
  navWrapperTabs: {

  },
  navWrapperVisibleActions: {

  },
}));

const ProjectsNavigation: FC = (): ReactElement => {
  const classes = useStyles();

  return (
    <section className={classes.navWrapper}>
      <div className={classes.navWrapperTabs}>
        Tabs
      </div>

      <div className={classes.navWrapperVisibleActions}>
        Visible actions
      </div>
    </section>
  );
};

export default ProjectsNavigation;