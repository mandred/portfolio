import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';
import CustomButton from '../../components/UI/CustomButton';
import {fontWeightEnum, ITheme} from '../../theme/types';
import {
  betweenChildrenMixin,
  flexLayoutMixin,
  oneLineTextMixin,
  sizeMixin
} from '../../theme/styleMixins';
import {IProject} from '../../store/reducers/projectsSlice';

const useStyles = createUseStyles((theme: ITheme) => ({
  projectListCard: {
    width: `${100 / 3}%`,
    height: '50vh',
    padding: theme.spacing(1),
    overflow: 'hidden',
    position: 'relative',
    borderRadius: theme.spacing(1),
    '&:hover': {
      '& $projectListCardDesc': {
        bottom: 0,
      },
    },
  },
  projectListCardImage: {
    ...sizeMixin('100%'),
    overflow: 'hidden',
    objectFit: 'cover',
    color: theme.palette.textPrimary,
    borderRadius: theme.spacing(1),
  },
  projectListCardDesc: {
    ...flexLayoutMixin('flex-start', 'center'),
    ...betweenChildrenMixin({
      marginBottom: theme.spacing(2),
    }),
    flexFlow: 'column nowrap',
    cursor: 'default',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: '-100%',
    backgroundColor: theme.palette.background,
    margin: theme.spacing(4),
    padding: theme.spacing(4),
    transition: 'all .3s linear',
    borderRadius: theme.spacing(1),
  },
  projectListCardDescTitle: {
    ...oneLineTextMixin(),
    color: theme.palette.textPrimary,
    fontSize: theme.fontSize(1.5),
    fontWeight: theme.fontWeight(fontWeightEnum.BOLD),
    transition: 'all .3s linear',
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
}));

const ProjectsCard: FC<IProjectsCardProps> = (props: IProjectsCardProps): ReactElement => {
  const classes = useStyles();
  const {imageSrc, imageName, title, demoURL, repositoryURL} = props.project;

  return (
    <section className={classes.projectListCard}>
      <img src={imageSrc} className={classes.projectListCardImage} alt={imageName}/>

      <div className={classes.projectListCardDesc}>
        <a href={repositoryURL} className={classes.projectListCardDescTitle} title={'Open repository'} target={'_blank'} rel="noreferrer">
          {title}
        </a>

        <CustomButton title={`Open project ${title}`} handler={() => window.open(`${demoURL}`, '_blank')}>
          Open project
        </CustomButton>
      </div>
    </section>
  );
};

export default ProjectsCard;

interface IProjectsCardProps {
  project: IProject,
}
