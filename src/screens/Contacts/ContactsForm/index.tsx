import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';
import {ITheme} from '../../../theme/types';
import CustomTextFieldInput from '../../../components/UI/CustomTextFiledInput';
import CustomTextFieldTextArea from '../../../components/UI/CustomTextFiledTextarea';
import CustomButton from '../../../components/UI/CustomButton';
import {betweenChildrenMixin, breakpointMixin, customScrollBarMixin, flexLayoutMixin} from '../../../theme/styleMixins';
import {WindowWidthRestrictionsEnum} from '../../../store/reducers/deviceInfoSlice/types';

const useStyles = createUseStyles((theme: ITheme) => ({
  contactsFormWrapper: {
    ...flexLayoutMixin('flex-start', 'flex-start'),
    ...betweenChildrenMixin({
      marginBottom: theme.spacing(4),
    }),
    flexFlow: 'column nowrap',
    padding: [theme.spacing(1), theme.spacing(2)],

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      ...customScrollBarMixin(theme.palette.textPrimary),
      height: '100%',
      flex: 1.5,
      overflow: 'hidden',
    }),
  },
  contactsFormWrapperRow: {
    ...flexLayoutMixin('center', 'space-between'),
    flexFlow: 'column nowrap',
    width: '100%',

    ...breakpointMixin(WindowWidthRestrictionsEnum.LARGE_MOBILE, 0, {
      flexFlow: 'row nowrap',
    }),
  },
  contactsFormWrapperTextFieldName: {
    marginBottom: theme.spacing(4),

    ...breakpointMixin(WindowWidthRestrictionsEnum.LARGE_MOBILE, 0, {
      marginRight: theme.spacing(4),
      marginBottom: 0,
    }),
  },
  contactsFormWrapperTextFieldEmail: {

  },
  submitButton: {

  },
}));

const ContactsForm: FC = (): ReactElement => {
  const classes = useStyles();

  return (
    <form className={classes.contactsFormWrapper}>
      <div className={classes.contactsFormWrapperRow}>
        <CustomTextFieldInput
          autoFocus
          type={'text'}
          placeholder={'Name'}
          name={'name'}
          title={'Enter your name'}
          classList={[classes.contactsFormWrapperTextFieldName]}/>

        <CustomTextFieldInput
          type={'email'}
          placeholder={'Email'}
          name={'email'}
          title={'Enter your email'}
          classList={[classes.contactsFormWrapperTextFieldEmail]}/>
      </div>

      <CustomTextFieldInput
        type={'text'}
        placeholder={'Subject'}
        name={'subject'}
        title={'Enter subject'}/>

      <CustomTextFieldTextArea
        rows={10}
        cols={100}
        autoCapitalize={'sentences'}
        placeholder={'Message'}
        name={'email message'}
        title={'Enter message'}/>

      <CustomButton handler={(e) => e.preventDefault()} type={'submit'} title={'Send message'} classes={[classes.submitButton]}>
        Send message
      </CustomButton>
    </form>
  );
};

export default ContactsForm;
