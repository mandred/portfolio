import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';
import {fontWeightEnum, ITheme} from '../../../theme/types';
import {ReactComponent as IconEmail} from './assets/iconEmail.svg';
import {ReactComponent as IconTelephone} from './assets/iconTelephone.svg';
import {ReactComponent as IconGithub} from './assets/iconGithub.svg';
import {ReactComponent as IconLinkedIn} from './assets/iconLinkedin.svg';
import {ReactComponent as IconVkontakte} from './assets/iconVk.svg';
import {ReactComponent as IconTelegram} from './assets/iconTelegram.svg';
import {betweenChildrenMixin, breakpointMixin, flexLayoutMixin, sizeMixin} from '../../../theme/styleMixins';
import {WindowWidthRestrictionsEnum} from '../../../store/reducers/deviceInfoSlice/types';
import classNames from 'classnames';
import ContactsInfoSocials from './ContactsInfoSocials';

const useStyles = createUseStyles((theme: ITheme) => ({
  contactsInfoWrapper: {
    ...flexLayoutMixin('flex-start', 'flex-start'),
    ...betweenChildrenMixin({
      marginBottom: theme.spacing(4),
    }),
    flexFlow: 'column nowrap',
    padding: [theme.spacing(1), theme.spacing(2)],
    marginBottom: theme.spacing(4),

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      height: '100%',
      flex: 1,
      marginBottom: 0,
      overflow: 'hidden',
    }),
  },
  contactsInfoWrapperItem: {
    ...flexLayoutMixin('flex-start', 'flex-start'),
    ...betweenChildrenMixin({
      marginRight: theme.spacing(3),
    }),
    flexFlow: 'row nowrap',
  },
  contactsInfoWrapperItemButtons: {
    flexFlow: 'row wrap',
    ...betweenChildrenMixin({
      marginBottom: theme.spacing(3),
    }),
  },
  contactsInfoWrapperItemDesc: {
    ...flexLayoutMixin('flex-start', 'flex-start'),
    ...betweenChildrenMixin({
      marginBottom: theme.spacing(2),
    }),
    flexFlow: 'column nowrap',
  },
  contactsInfoWrapperItemDescIcon: {
    ...sizeMixin(theme.spacing(5)),
    fill: theme.palette.primary,
  },
  contactsInfoWrapperItemDescText: {
    flex: 1,
    color: theme.palette.textPrimary,
    fontWeight: theme.fontWeight(fontWeightEnum.BOLD),
  },
  contactsInfoWrapperItemDescLink: {
    flex: 1,
    color: theme.palette.textPrimary,
    transition: 'color .3s linear',
    '&:hover': {
      color: theme.palette.primary,
    },
  },
}));

const ContactsInfo: FC = (): ReactElement => {
  const classes = useStyles();
  const {REACT_APP_EMAIL, REACT_APP_TELEPHONE} = process.env;

  return (
    <div className={classes.contactsInfoWrapper}>
      <div className={classes.contactsInfoWrapperItem} title={`${REACT_APP_EMAIL}`}>
        <IconEmail className={classes.contactsInfoWrapperItemDescIcon} />

        <div className={classes.contactsInfoWrapperItemDesc}>
          <p className={classes.contactsInfoWrapperItemDescText}>
            E-mail
          </p>

          <a href={`mailto:${REACT_APP_EMAIL}`} className={classes.contactsInfoWrapperItemDescLink}>
            {REACT_APP_EMAIL}
          </a>
        </div>
      </div>

      <div className={classes.contactsInfoWrapperItem} title={`${REACT_APP_TELEPHONE}`}>
        <IconTelephone className={classes.contactsInfoWrapperItemDescIcon} />

        <div className={classes.contactsInfoWrapperItemDesc}>
          <p className={classes.contactsInfoWrapperItemDescText}>
            Telephone
          </p>

          <a href={`tel:${REACT_APP_TELEPHONE}`} className={classes.contactsInfoWrapperItemDescLink}>
            {REACT_APP_TELEPHONE}
          </a>
        </div>
      </div>

      <div className={classNames([classes.contactsInfoWrapperItem, classes.contactsInfoWrapperItemButtons])}>
        <ContactsInfoSocials />
      </div>
    </div>
  );
};

export default ContactsInfo;
