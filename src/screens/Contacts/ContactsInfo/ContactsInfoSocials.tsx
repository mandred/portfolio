import React, {FC, ReactElement, ReactNode} from 'react';
import {createUseStyles} from 'react-jss';
import {ITheme} from '../../../theme/types';
import {ReactComponent as IconGithub} from './assets/iconGithub.svg';
import {ReactComponent as IconLinkedIn} from './assets/iconLinkedin.svg';
import {ReactComponent as IconVkontakte} from './assets/iconVk.svg';
import {ReactComponent as IconTelegram} from './assets/iconTelegram.svg';
import {flexLayoutMixin, sizeMixin} from '../../../theme/styleMixins';

const useStyles = createUseStyles((theme: ITheme) => ({
  contactsInfoWrapperButton: {
    ...flexLayoutMixin(),
    ...sizeMixin(60),
    border: 'none',
    borderRadius: '50%',
    backgroundColor: theme.palette.buttonBackground,
    transition: 'all .25s linear',
    '&:hover': {
      cursor: 'pointer',
      '& $contactsInfoWrapperButtonIcon': {
        fill: theme.palette.primary,
      },
    },
  },
  contactsInfoWrapperButtonIcon: {
    ...flexLayoutMixin(),
    ...sizeMixin(theme.spacing(4)),
    fill: theme.palette.iconPrimary,
    transition: 'all .25s linear',
  },
}));

const ContactsInfoSocials: FC = (): ReactElement => {
  const classes = useStyles();

  const {REACT_APP_GIT_HUB, REACT_APP_VK, REACT_APP_TELEGRAM, REACT_APP_LINKEDIN} = process.env;

  const socialItemList: ISocialItem[] = [
    {
      name: 'GitHub',
      address: `${REACT_APP_GIT_HUB}`,
      icon: <IconGithub className={classes.contactsInfoWrapperButtonIcon} />,
    },
    {
      name: 'LinkedIn',
      address: `${REACT_APP_LINKEDIN}`,
      icon: <IconLinkedIn className={classes.contactsInfoWrapperButtonIcon} />,
    },
    {
      name: 'VKontakte',
      address: `${REACT_APP_VK}`,
      icon: <IconVkontakte className={classes.contactsInfoWrapperButtonIcon} />,
    },
    {
      name: 'Telegram',
      address: `${REACT_APP_TELEGRAM}`,
      icon: <IconTelegram className={classes.contactsInfoWrapperButtonIcon} />,
    },
  ];

  const openSocialItemAddress = (address: string): void => {
    window.open(address, '_blank');
  };

  return (
    <>
      {
        socialItemList.map((item) => (
          <button
            key={item.name}
            onClick={() => openSocialItemAddress(item.address)}
            title={`${item.name}: ${item.address}`}
            className={classes.contactsInfoWrapperButton}>
            {item.icon}
          </button>
        ))
      }
    </>
  );
};

export default ContactsInfoSocials;

interface ISocialItem {
  name: string,
  address: string,
  icon: ReactNode,
}
