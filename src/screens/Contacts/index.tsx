import React, {FC, ReactElement, useEffect} from 'react';
import {fetchUserContacts} from '../../store/actions/userContactsActions';
import {useAppDispatch} from '../../store/hooks';
import {createUseStyles} from 'react-jss';
import ScreenTitle from '../../components/ScreenTitle';
import {ITheme} from '../../theme/types';
import ContactsInfo from './ContactsInfo';
import ContactsForm from './ContactsForm';
import {
  breakpointMixin,
  customScrollBarMixin,
  flexLayoutMixin,
  sizeMixin
} from '../../theme/styleMixins';
import {WindowWidthRestrictionsEnum} from '../../store/reducers/deviceInfoSlice/types';

const useStyles = createUseStyles((theme: ITheme) => ({
  contactsWrapper: {
    ...sizeMixin('100%'),
    ...flexLayoutMixin('flex-start', 'flex-start'),
    ...customScrollBarMixin(theme.palette.textPrimary),
    flexFlow: 'column nowrap',

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      ...flexLayoutMixin('center', 'center'),
      flexFlow: 'row nowrap',
      overflowY: 'hidden',
    }),
  },
}));

const Contacts: FC = (): ReactElement => {
  const classes = useStyles();
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchUserContacts());
  }, []);

  return (
    <>
      <ScreenTitle title={'Contacts'}/>

      <section className={classes.contactsWrapper}>
        <ContactsInfo />

        <ContactsForm />
      </section>
    </>
  );
};

export default Contacts;
