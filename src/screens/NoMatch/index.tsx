import React, {FC, ReactElement} from 'react';
import {useHistory} from 'react-router-dom';
import {createUseStyles} from 'react-jss';
import CustomButton from '../../components/UI/CustomButton';
import {ITheme} from '../../theme/types';
import {betweenChildrenMixin, flexLayoutMixin, sizeMixin} from '../../theme/styleMixins';

const useStyles = createUseStyles((theme: ITheme) => ({
  wrapper: {
    ...sizeMixin('100%'),
    ...flexLayoutMixin(),
    ...betweenChildrenMixin({
      marginBottom: theme.spacing(4),
    }),
    flexFlow: 'column nowrap',
  },
  title: {
    fontSize: theme.fontSize(2),
    color: theme.palette.textPrimary,
  },
}));

const NoMatch: FC = (): ReactElement => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <section className={classes.wrapper}>
      <h1 className={classes.title}>There is nothing interesting here</h1>

      <CustomButton handler={() => history.push('/')} title={'Go home'}>
        Go home
      </CustomButton>
    </section>
  );
};

export default NoMatch;
