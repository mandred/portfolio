import React, {FC, ReactElement} from 'react';
import {createUseStyles} from 'react-jss';
import {fontWeightEnum, ITheme} from '../../theme/types';
import {flexLayoutMixin} from '../../theme/styleMixins';

const useStyles = createUseStyles((theme: ITheme) => ({
  aboutInfoCard: {
    ...flexLayoutMixin('center', 'flex-start'),
    color: theme.palette.textPrimary,
    fontSize: theme.fontSize(1.5),
  },
  aboutInfoCardKey: {
    position: 'relative',
    marginRight: theme.spacing(2),
    '&::after': {
      content: '":"',
      position: 'absolute',
      top: '50%',
      transform: 'translateY(-50%)',
      right: theme.spacing(1) - theme.spacing(2),
    },
  },
  aboutInfoCardValue: {
    fontWeight: theme.fontWeight(fontWeightEnum.BOLD),
  },
}));

const AboutInfoCard: FC<IAboutInfoCardProps> = (props: IAboutInfoCardProps): ReactElement => {
  const classes = useStyles();
  const {infoKey, infoValue} = props;

  return (
    <section className={classes.aboutInfoCard}>
      <p className={classes.aboutInfoCardKey}>
        {infoKey}
      </p>

      <p className={classes.aboutInfoCardValue}>
        {infoValue}
      </p>
    </section>
  );
};

export default AboutInfoCard;

interface IAboutInfoCardProps {
  infoKey: string,
  infoValue: string,
}
