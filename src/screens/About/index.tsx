import React, {FC, ReactElement, useEffect} from 'react';
import {useAppDispatch, useAppSelector} from '../../store/hooks';
import {fetchUserContacts} from '../../store/actions/userContactsActions';
import {createUseStyles} from 'react-jss';
import ScreenTitle from '../../components/ScreenTitle';
import {betweenChildrenMixin, flexLayoutMixin, sizeMixin} from '../../theme/styleMixins';
import {fontWeightEnum, ITheme} from '../../theme/types';
import AboutInfoCard from './AboutInfoCard';
import CustomPreloader from '../../components/UI/CustomPreloader';
import CustomButton from '../../components/UI/CustomButton';
import {fetchAvatarImage} from '../../store/actions/avatarImageActions';
import {fetchMainInfo} from '../../store/actions/mainInfoActions';

const useStyles = createUseStyles((theme: ITheme) => ({
  aboutWrapper: {
    ...sizeMixin('100%'),
    ...flexLayoutMixin(),
  },
  photoWrapper: {
    ...sizeMixin('100%'),
    ...flexLayoutMixin('flex-start', 'center'),
    padding: [[theme.spacing(3), theme.spacing(14), 0, theme.spacing(14)]],
    overflow: 'hidden',
  },
  descWrapper: {
    ...sizeMixin('100%'),
    ...flexLayoutMixin('flex-start', 'flex-start'),
    flexFlow: 'column nowrap',
    padding: [[theme.spacing(3), theme.spacing(14), 0, theme.spacing(14)]],
  },
  descWrapperTitle: {
    color: theme.palette.textPrimary,
    fontSize: theme.fontSize(1.5),
    fontWeight: theme.fontWeight(fontWeightEnum.BOLD),
    marginBottom: theme.spacing(3),
    textTransform: 'uppercase',
  },
  aboutInfoColumns: {
    ...flexLayoutMixin('flex-start', 'space-between'),
    ...betweenChildrenMixin({
      marginRight: theme.spacing(6),
    }),
    marginBottom: theme.spacing(10),
    flexFlow: 'row nowrap',
  },
  aboutInfoOne: {
    ...betweenChildrenMixin({
      marginBottom: theme.spacing(4),
    }),
  },
  aboutInfoTwo: {
    ...betweenChildrenMixin({
      marginBottom: theme.spacing(4),
    }),
  },
  photo: {
    ...sizeMixin(300),
    borderRadius: '50%',
    backgroundColor: theme.palette.primary,
    color: theme.palette.textPrimary,
    objectFit: 'contain',
  },
  bioButton: {},
}));

const getAge = (input: string): number => {
  const d = input.split('.');
  return typeof d[2] !== 'undefined'
    ? ((new Date().getTime() - new Date(`${d[2]}.${d[1]}.${d[0]}`).getTime()) / (24 * 3600 * 365.25 * 1000)) | 0
    : 0;
};

const About: FC = (): ReactElement => {
  const classes = useStyles();
  const {pending} = useAppSelector((state) => state.userContactsSlice);
  const {data: avatarImageData, pending: avatarImagePending} = useAppSelector((state) => state.avatarImageSlice);
  const {data: mainInfoData} = useAppSelector((state) => state.mainInfoSlice);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchUserContacts());
    dispatch(fetchMainInfo());

    if (!avatarImageData.fileSrc) {
      dispatch(fetchAvatarImage());
    }
  }, []);

  return (
    <>
      <ScreenTitle title={'About'}/>

      {
        pending
          ? <CustomPreloader type={'balls'}/>
          : <section className={classes.aboutWrapper}>
              <section className={classes.photoWrapper}>
                {
                  avatarImagePending
                    ? <CustomPreloader type={'circles'} />
                    : <img src={avatarImageData.fileSrc} alt={avatarImageData.fileName} className={classes.photo}/>
                }
              </section>

              <section className={classes.descWrapper}>
                <h5 className={classes.descWrapperTitle}>
                  Personal info
                </h5>

                <div className={classes.aboutInfoColumns}>
                  <div className={classes.aboutInfoOne}>
                    <AboutInfoCard infoKey={'First name'} infoValue={'Alexander'} />
                    <AboutInfoCard infoKey={'Age'} infoValue={`${getAge('19.12.1991')}`} />
                    <AboutInfoCard infoKey={'Skype'} infoValue={'alexander1991'} />
                  </div>

                  <div className={classes.aboutInfoTwo}>
                    <AboutInfoCard infoKey={'Last name'} infoValue={'Alexeev'} />
                    <AboutInfoCard infoKey={'Nationality'} infoValue={'Ukrainian'} />
                    <AboutInfoCard infoKey={'Email'} infoValue={'mandred1991@gmail.com'} />
                  </div>
                </div>

                <CustomButton handler={() => window.open(mainInfoData.fileSrc, '_blank')} title={'Open BIO'} classes={[classes.bioButton]}>
                  Open BIO
                </CustomButton>
              </section>
            </section>
      }
    </>
  );
};

export default About;
