import React, {FC, ReactElement, useEffect} from 'react';
import {createUseStyles} from 'react-jss';
import {useHistory} from 'react-router-dom';
import CustomButton from '../../components/UI/CustomButton';
import CustomPreloader from '../../components/UI/CustomPreloader';
import {fontWeightEnum, ITheme} from '../../theme/types';
import {
  betweenChildrenMixin,
  breakpointMixin,
  customScrollBarMixin,
  flexLayoutMixin,
  sizeMixin
} from '../../theme/styleMixins';
import {useAppDispatch, useAppSelector} from '../../store/hooks';
import {fetchMainImage} from '../../store/actions/mainImageActions';
import {WindowWidthRestrictionsEnum} from '../../store/reducers/deviceInfoSlice/types';

const useStyles = createUseStyles((theme: ITheme) => ({
  homeWrapper: {
    ...customScrollBarMixin(theme.palette.textPrimary), // TODO BUG вызывается только после одной смены темы, потом перестает вызываться
    ...flexLayoutMixin('center', 'flex-start'),
    ...sizeMixin('100%'),
    flexFlow: 'column nowrap',

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      ...flexLayoutMixin('center', 'center'),
      flexFlow: 'row nowrap',
    }),
  },
  descWrapper: {
    ...sizeMixin('100%'),
    ...flexLayoutMixin('center', 'center'),
    flexFlow: 'column nowrap',
    order: 2,
    padding: [[theme.spacing(6), 0, theme.spacing(2), 0]],

    ...breakpointMixin(WindowWidthRestrictionsEnum.DESKTOP, 0, {
      ...flexLayoutMixin('flex-start', 'center'),
      padding: [[theme.spacing(9), 0, theme.spacing(2), theme.spacing(9)]],
      order: 1,
    }),
  },
  title: {
    paddingLeft: theme.spacing(5),
  },
  titleOne: {
    marginBottom: theme.spacing(3),
    fontSize: theme.fontSize(3.15),
    fontWeight: theme.fontWeight(fontWeightEnum.BOLD),
    color: theme.palette.primary,
    position: 'relative',
    '&::before': {
      content: '""',
      position: 'absolute',
      width: theme.spacing(3),
      height: 4,
      backgroundColor: theme.palette.primary,
      left: `-${theme.spacing(5)}px`,
      top: '50%',
      transform: 'translateY(-50%)',
    }
  },
  titleTwo: {
    marginBottom: theme.spacing(3),
    fontSize: theme.fontSize(3.15),
    fontWeight: theme.fontWeight(fontWeightEnum.BOLD),
    color: theme.palette.textPrimary,
  },
  subtitle: {
    marginBottom: theme.spacing(4),
    fontSize: theme.fontSize(1.15),
    color: theme.palette.textPrimary,
    textAlign: 'center',

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      textAlign: 'start',
    }),
  },
  mainButton: {
    alignSelf: 'inherit',

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      alignSelf: 'flex-start',
    }),
  },
  socials: {
    marginTop: 'auto',
    width: '100%',
    ...flexLayoutMixin('center', 'flex-start'),
    ...betweenChildrenMixin({
      marginRight: theme.spacing(3),
    }),
  },
  photoWrapper: {
    ...sizeMixin(300),
    ...flexLayoutMixin(),
    order: 1,
    padding: 0,
    overflow: 'visible',

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      ...sizeMixin('100%'),
      padding: [0, theme.spacing(14)],
      overflow: 'hidden',
      order: 2,
    }),
  },
  photo: {
    ...sizeMixin(300),
    color: theme.palette.textPrimary,
    objectFit: 'contain',

    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      height: '100%',
      width: 'auto',
    }),
  },
}));

const Home: FC = (): ReactElement => {
  const classes = useStyles();
  const dispatch = useAppDispatch();
  const {data: mainImageData, pending: mainImagePending} = useAppSelector((state) => state.mainImageSlice);
  const {window: deviceInfoWindow} = useAppSelector((state) => state.deviceInfoSlice);
  const history = useHistory();

  const {REACT_APP_GIT_HUB, REACT_APP_VK, REACT_APP_LINKEDIN} = process.env;

  useEffect(() => {
    if (!mainImageData.fileSrc) {
      dispatch(fetchMainImage());
    }
  }, []);

  const openSocialNetwork = (network: string) => {
    switch (network) {
      case 'gitHub': window.open(`${REACT_APP_GIT_HUB}`, '_blank'); break;
      case 'linkedIn': window.open(`${REACT_APP_LINKEDIN}`, '_blank'); break;
      case 'vKontakte': window.open(`${REACT_APP_VK}`, '_blank'); break;
      default: break;
    }
  };

  return (
    <section className={classes.homeWrapper}>
      <section className={classes.descWrapper}>
        <h1 className={classes.title}>
          <p className={classes.titleOne}>
            {/* eslint-disable-next-line react/no-unescaped-entities */}
            I'm Alexander Alexeev
          </p>
          <p className={classes.titleTwo}>
            Frontend developer
          </p>
        </h1>

        <p className={classes.subtitle}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem inventore ipsum obcaecati quia repudiandae sequi.
        </p>

        <CustomButton handler={() => history.push('/projects')} title={'See my projects'} classes={[classes.mainButton]}>See my projects</CustomButton>

        {
          deviceInfoWindow.width > WindowWidthRestrictionsEnum.TABLET && <div className={classes.socials}>
            <CustomButton handler={() => openSocialNetwork('gitHub')} title={`GitHub: ${REACT_APP_GIT_HUB}`}>
              GitHub
            </CustomButton>
            <CustomButton handler={() => openSocialNetwork('linkedIn')} title={`LinkedIn: ${REACT_APP_LINKEDIN}`}>
              LinkedIn
            </CustomButton>
            <CustomButton handler={() => openSocialNetwork('vKontakte')} title={`VKontakte: ${REACT_APP_VK}`}>
              VKontakte
            </CustomButton>
          </div>
        }
      </section>

      <section className={classes.photoWrapper}>
        {
          mainImagePending
            ? <CustomPreloader type={'circles'} />
            : <img src={mainImageData.fileSrc} alt={mainImageData.fileName} className={classes.photo}/>
        }
      </section>
    </section>
  );
};

export default Home;
