import React, {FC, ReactElement} from 'react';
import {createUseStyles, useTheme} from 'react-jss';
import ProgressBar from 'react-customizable-progressbar';
import {fontWeightEnum, ITheme} from '../../theme/types';
import {betweenChildrenMixin, flexLayoutMixin, sizeMixin} from '../../theme/styleMixins';
import {ISkill} from '../../store/reducers/skillsSlice';

const useStyles = createUseStyles((theme: ITheme) => ({
  skillsCardWrapper: {
    ...flexLayoutMixin('center', 'center'),
    width: `${100 / 5}%`,
    padding: theme.spacing(2),
    overflow: 'hidden',
  },
  skillsCardInfo: {
    ...flexLayoutMixin('center', 'center'),
    ...betweenChildrenMixin({
      marginBottom: theme.spacing(3),
    }),
    flexFlow: 'column nowrap',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    margin: '0 auto',
    userSelect: 'none',
  },
  skillsCardInfoIcon: {
    ...flexLayoutMixin('center', 'center'),
    ...sizeMixin(90),
    color: theme.palette.textPrimary,
  },
  skillsCardInfoTitle: {
    color: theme.palette.textPrimary,
    fontSize: theme.fontSize(1.5),
    fontWeight: theme.fontWeight(fontWeightEnum.BOLD),
  },
}));

const SkillsCard: FC<ISkillsCardProps> = (props: ISkillsCardProps): ReactElement => {
  const classes = useStyles();
  const theme = useTheme<ITheme>();
  const {title, mainColor, progress, imageSrc, imageName} = props.skill;

  return (
    <section className={classes.skillsCardWrapper} title={`${title} - ${progress}%`}>
      <ProgressBar
      radius={100}
      progress={Number(progress)}
      strokeWidth={theme.spacing(3)}
      strokeColor={mainColor}
      strokeLinecap="butt"
      trackStrokeWidth={14}
      trackStrokeLinecap="butt"
      cut={120}
      rotate={-210}
      initialAnimation
      initialAnimationDelay={200}>
        <div className={classes.skillsCardInfo}>
          <img src={imageSrc} className={classes.skillsCardInfoIcon} alt={imageName} />

          <div className={classes.skillsCardInfoTitle}>
            {title}
          </div>
        </div>
      </ProgressBar>
    </section>
  );
};

export default SkillsCard;

interface ISkillsCardProps {
  skill: ISkill,
}
