import React, {FC, ReactElement, useEffect} from 'react';
import {createUseStyles} from 'react-jss';
import {useAppDispatch, useAppSelector} from '../../store/hooks';
import {fetchSkills} from '../../store/actions/skillsActions';
import ScreenTitle from '../../components/ScreenTitle';
import {flexLayoutMixin, sizeMixin} from '../../theme/styleMixins';
import CustomPreloader from '../../components/UI/CustomPreloader';
import SkillsCard from './SkillsCard';

const useStyles = createUseStyles(() => ({
  skillsWrapper: {
    ...sizeMixin('100%'),
    ...flexLayoutMixin(),
    flexFlow: 'column nowrap',
    overflow: 'hidden',
  },
  skillsList: {
    ...flexLayoutMixin('flex-start', 'flex-start'),
    ...sizeMixin('100%'),
    flexFlow: 'row wrap',
    overflowY: 'auto',
  },
}));

const Skills: FC = (): ReactElement => {
  const classes = useStyles();
  const {skills, pending} = useAppSelector((state) => state.skillsSlice);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!skills.length) {
      dispatch(fetchSkills());
    }
  }, []);

  return (
    <>
      <ScreenTitle title={'Skills'}/>

      {
        pending
          ? <CustomPreloader type={'balls'}/>
          : <section className={classes.skillsWrapper}>
              <div className={classes.skillsList}>
                {
                  skills
                    .filter((item) => item.isVisible)
                    .map((item) => <SkillsCard skill={item} key={item.title}/>)
                }
              </div>
          </section>
      }
    </>
  );
};

export default Skills;
