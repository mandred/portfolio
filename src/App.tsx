import React, {FC, ReactElement, useEffect} from 'react';
import {BrowserRouter} from 'react-router-dom';
import {ThemeProvider} from 'react-jss';
import Router from './Router';
import {useAppDispatch, useAppSelector} from './store/hooks';
import {themeLight, themeDark} from './theme';
import {themeTypeEnum} from './theme/types';
import {updateInfo} from './store/actions/deviceInfoActions';

const App: FC = (): ReactElement => {
  const {theme} = useAppSelector((state) => state.themeSlice);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(updateInfo());
  }, []);

  return (
    <ThemeProvider theme={theme === themeTypeEnum.LIGHT ? themeLight : themeDark}>
      <BrowserRouter>
        <Router/>
      </BrowserRouter>
    </ThemeProvider>
  );
};

export default App;
