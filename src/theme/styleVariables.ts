import {fontWeightEnum, IStyleVariables, IPalette} from './types';

export const styleVariablesBase: IStyleVariables = {
  spacing: 8,
  fontSize: 16,
  fontWeight: fontWeightEnum.NORMAL,
};

export const styleVariablesPaletteLight: IPalette = {
  primary: '#72b626',
  secondary: '#000', // no usage
  background: '#fff',
  backgroundDisabled: '#000', // no usage
  buttonBackground: '#eee',
  buttonHover: '#f9f9f9',
  textPrimary: '#666',
  textSecondary: '#fff',
  textDisabled: '#000', // no usage
  textError: '#000', // no usage
  iconPrimary: '#666',
  iconSecondary: '#fff',
  iconThemeButton: '#bdd0e4',
  iconDisabled: '#000', // no usage
};

export const styleVariablesPaletteDark: IPalette = {
  primary: '#ffb400',
  secondary: '#000', // no usage
  background: '#111',
  backgroundDisabled: '#000', // no usage
  buttonBackground: '#2b2a2a',
  buttonHover: '#202020',
  textPrimary: '#fff',
  textSecondary: '#fff',
  textDisabled: '#000', // no usage
  textError: '#000', // no usage
  iconPrimary: '#ddd',
  iconSecondary: '#fff',
  iconThemeButton: '#fff917',
  iconDisabled: '#000', // no usage
};
