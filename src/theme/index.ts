import {ITheme, IThemeBase} from './types';
import {styleVariablesBase, styleVariablesPaletteDark, styleVariablesPaletteLight} from './styleVariables';

const themeBase: IThemeBase = {
  spacing: (modifier ) => styleVariablesBase.spacing * modifier,
  fontSize: (modifier ) => styleVariablesBase.fontSize * modifier,
  fontWeight: (fontWeight) => fontWeight,
};

export const themeLight: ITheme = {
  ...themeBase,
  palette: {...styleVariablesPaletteLight},
};

export const themeDark: ITheme = {
  ...themeBase,
  palette: {...styleVariablesPaletteDark},
};
