export interface IStyleVariables {
  spacing: number,
  fontSize: number,
  fontWeight: fontWeightEnum,
}

export interface IPalette {
  primary: string,
  secondary: string,
  background: string,
  backgroundDisabled: string,
  buttonBackground: string,
  buttonHover: string,
  textPrimary: string,
  textSecondary: string,
  textDisabled: string,
  textError: string,
  iconPrimary: string,
  iconSecondary: string,
  iconThemeButton: string,
  iconDisabled: string,
}

export enum themeTypeEnum {
  LIGHT = 'LIGHT',
  DARK = 'DARK',
}

export enum fontWeightEnum {
  EXTRA_LIGHT = 200,
  NORMAL = 400,
  BOLD = 700,
  EXTRA_BOLD = 900,
}

export interface IThemeBase {
  spacing: (modifier: number) => number,
  fontSize: (modifier: number) => number,
  fontWeight: (fontWeight: fontWeightEnum) => number,
}

export interface ITheme extends IThemeBase {
  palette: IPalette,
}
