import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {fetchMainInfo} from '../actions/mainInfoActions';

const initialState: IStateMainInfo = {
  data: {} as IMainInfo,
  pending: false,
  error: '',
};

export const mainInfoSlice = createSlice({
  name: 'mainInfo',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchMainInfo.pending.type]: (state) => {
      state.pending = true;
    },
    [fetchMainInfo.fulfilled.type]: (state, action: PayloadAction<IMainInfo>) => {
      state.error = '';
      state.data = action.payload;
      state.pending = false;
    },
    [fetchMainInfo.rejected.type]: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
      state.pending = false;
    }
  }
});

export default mainInfoSlice.reducer;

export interface IStateMainInfo {
  data: IMainInfo,
  pending: boolean,
  error: string,
}

export interface IMainInfo {
  fileName: string,
  fileSrc: string,
}
