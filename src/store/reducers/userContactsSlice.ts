import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {fetchUserContacts} from '../actions/userContactsActions';

const initialState: IStateUserContacts = {
  userContacts: [],
  pending: false,
  error: '',
};

export const userContactsSlice = createSlice({
  name: 'userContacts',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchUserContacts.pending.type]: (state) => {
      state.pending = true;
    },
    [fetchUserContacts.fulfilled.type]: (state, action: PayloadAction<Array<IUserContact>>) => {
      state.error = '';
      state.userContacts = action.payload;
      state.pending = false;
    },
    [fetchUserContacts.rejected.type]: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
      state.pending = false;
    }
  }
});

export default userContactsSlice.reducer;

export interface IStateUserContacts {
  userContacts: Array<IUserContact>,
  pending: boolean,
  error: string,
}

export interface IUserContact {
  imageName: string,
  imageSrc: string,
  title: string,
  url: string,
  id: string,
}
