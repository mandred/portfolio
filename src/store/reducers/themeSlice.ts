import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {toggleTheme} from '../actions/themeActions';
import {themeTypeEnum} from '../../theme/types';

const initialState: {theme: themeTypeEnum} = {
  theme: localStorage.getItem('appTheme') as themeTypeEnum === themeTypeEnum.DARK ? themeTypeEnum.DARK : themeTypeEnum.LIGHT,
};

export const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {},
  extraReducers: {
    [toggleTheme.fulfilled.type]: (state, action: PayloadAction<string>) => {
      state.theme = action.payload as themeTypeEnum;
    },
  }
});

export default themeSlice.reducer;
