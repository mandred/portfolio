import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {fetchAvatarImage} from '../actions/avatarImageActions';

const initialState: IStateAvatarImage = {
  data: {} as IAvatarImage,
  pending: false,
  error: '',
};

export const mainImageSlice = createSlice({
  name: 'mainImage',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchAvatarImage.pending.type]: (state) => {
      state.pending = true;
    },
    [fetchAvatarImage.fulfilled.type]: (state, action: PayloadAction<IAvatarImage>) => {
      state.error = '';
      state.data = action.payload;
      state.pending = false;
    },
    [fetchAvatarImage.rejected.type]: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
      state.pending = false;
    }
  }
});

export default mainImageSlice.reducer;

export interface IStateAvatarImage {
  data: IAvatarImage,
  pending: boolean,
  error: string,
}

export interface IAvatarImage {
  fileSrc: string,
  fileName: string,
}
