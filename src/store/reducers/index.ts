import {combineReducers} from '@reduxjs/toolkit';
import projectsSlice from './projectsSlice';
import skillsSlice from './skillsSlice';
import userContactsSlice from './userContactsSlice';
import mainImageSlice from './mainImageSlice';
import mainInfoSlice from './mainInfoSlice';
import avatarImageSlice from './avatarImageSlice';
import themeSlice from './themeSlice';
import deviceInfoSlice from './deviceInfoSlice';

export const rootReducer = combineReducers({
  projectsSlice,
  skillsSlice,
  userContactsSlice,
  mainImageSlice,
  avatarImageSlice,
  mainInfoSlice,
  themeSlice,
  deviceInfoSlice,
});
