import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {
  IBrowserInfo,
  IDeviceInfoSlice,
  IPlatformInfo,
  ISystemInfo,
  IWindowInfo,
  WindowWidthRestrictionsEnum
} from './types';

const initialState: IDeviceInfoSlice = {
  platform: {} as IPlatformInfo,
  system: {} as ISystemInfo,
  browser: {} as IBrowserInfo,
  window: {} as IWindowInfo,
  windowWidthRestrictions: {
    desktop: WindowWidthRestrictionsEnum.DESKTOP,
    tablet: WindowWidthRestrictionsEnum.TABLET,
    mobile: WindowWidthRestrictionsEnum.MOBILE,
  },
  windowSizeWatcherEnabled: false,
};

export const deviceInfoSlice = createSlice({
  name: 'deviceInfo',
  initialState,
  reducers: {
    setPlatformInfo(state, action: PayloadAction<IPlatformInfo>) {
      state.platform = action.payload;
    },
    setSystemInfo(state, action: PayloadAction<ISystemInfo>) {
      state.system = action.payload;
    },
    setBrowserInfo(state, action: PayloadAction<IBrowserInfo>) {
      state.browser = action.payload;
    },
    setWindowInfo(state, action: PayloadAction<IWindowInfo>) {
      state.window = action.payload;
    },
    setWindowSizeWatcherEnabled(state, action: PayloadAction<boolean>) {
      state.windowSizeWatcherEnabled = action.payload;
    },
  },
  extraReducers: {}
});

export default deviceInfoSlice.reducer;
