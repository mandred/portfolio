export interface IDeviceInfoSlice {
  platform: IPlatformInfo,
  system: ISystemInfo,
  browser: IBrowserInfo,
  window: IWindowInfo,
  windowWidthRestrictions: IWindowWidthRestrictions,
  windowSizeWatcherEnabled: boolean,
}

export interface IPlatformInfo {
  type?: string,
  vendor?: string,
  model?: string,
  desktop: boolean,
  tablet: boolean,
  mobile: boolean,
  iPhone: boolean,
  iPad: boolean,
  touch: boolean,
}

export interface ISystemInfo {
  id?: string,
  name?: string,
  version?: string,
  versionName?: string,
  majorVersion: number | null,
  windows: boolean,
  linux: boolean,
  mac: boolean,
  ios: boolean,
  android: boolean,
  screenResolution: IScreenResolution,
}

interface IScreenResolution {
  value: string,
  width: number,
  height: number,
}

export interface IBrowserInfo {
  id?: string,
  name?: string,
  vendor: string,
  version?: string,
  majorVersion: number | null,
  userAgent: string,
  chrome: boolean,
  firefox: boolean,
  safari: boolean,
  yandex: boolean,
  opera: boolean,
  edge: boolean,
  ie: boolean,
  samsung: boolean,
  language: string,
  locale: string,
  timezone: string,
  cookieEnabled: boolean,
  indexedDbEnabled: boolean,
  localStorageEnabled: boolean,
}

export interface IWindowInfo {
  width: number,
  height: number,
  desktop: boolean,
  tablet: boolean,
  mobile: boolean,
}

export enum WindowWidthRestrictionsEnum {
  DESKTOP = 1280,
  LARGE_TABLET = 960,
  TABLET = 768,
  LARGE_MOBILE = 540,
  MOBILE = 320,
}

export interface IWindowWidthRestrictions {
  desktop: WindowWidthRestrictionsEnum.DESKTOP,
  tablet: WindowWidthRestrictionsEnum.TABLET,
  mobile: WindowWidthRestrictionsEnum.MOBILE,
}
