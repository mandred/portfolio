import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {fetchMainImage} from '../actions/mainImageActions';

const initialState: IStateMainImage = {
  data: {} as IMainImage,
  pending: false,
  error: '',
};

export const mainImageSlice = createSlice({
  name: 'mainImage',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchMainImage.pending.type]: (state) => {
      state.pending = true;
    },
    [fetchMainImage.fulfilled.type]: (state, action: PayloadAction<IMainImage>) => {
      state.error = '';
      state.data = action.payload;
      state.pending = false;
    },
    [fetchMainImage.rejected.type]: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
      state.pending = false;
    }
  }
});

export default mainImageSlice.reducer;

export interface IStateMainImage {
  data: IMainImage,
  pending: boolean,
  error: string,
}

export interface IMainImage {
  fileSrc: string,
  fileName: string,
}

