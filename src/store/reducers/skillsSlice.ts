import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {fetchSkills} from '../actions/skillsActions';

const initialState: IStateSkills = {
  skills: [],
  pending: false,
  error: '',
};

export const skillsSlice = createSlice({
  name: 'skills',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchSkills.pending.type]: (state) => {
      state.pending = true;
    },
    [fetchSkills.fulfilled.type]: (state, action: PayloadAction<Array<ISkill>>) => {
      state.error = '';
      state.skills = action.payload;
      state.pending = false;
    },
    [fetchSkills.rejected.type]: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
      state.pending = false;
    }
  }
});

export default skillsSlice.reducer;

export interface IStateSkills {
  skills: Array<ISkill>,
  pending: boolean,
  error: string,
}

export interface ISkill {
  imageName: string,
  imageSrc: string,
  isVisible: boolean,
  mainColor: string,
  progress: string,
  title: string,
  id: string,
}
