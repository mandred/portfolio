import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {fetchProjects} from '../actions/projectsActions';

const initialState: IStateProjects = {
  projects: [],
  pending: false,
  error: '',
};

export const projectsSlice = createSlice({
  name: 'projects',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchProjects.pending.type]: (state) => {
      state.pending = true;
    },
    [fetchProjects.fulfilled.type]: (state, action: PayloadAction<Array<IProject>>) => {
      state.error = '';
      state.projects = action.payload;
      state.pending = false;
    },
    [fetchProjects.rejected.type]: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
      state.pending = false;
    }
  }
});

export default projectsSlice.reducer;

export interface IStateProjects {
  projects: Array<IProject>,
  pending: boolean,
  error: string,
}

export interface IProject {
  demoURL: string,
  description?: string,
  id: string,
  imageName?: string,
  imageSrc?: string,
  isVisible: boolean,
  mainTechnology: string,
  releaseDate: Date| string,
  repositoryURL: string,
  technologies?: Array<string>,
  title: string,
}
