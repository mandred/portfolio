import {createAsyncThunk} from '@reduxjs/toolkit';
import {fetchData} from '../utils';
import {IUserContact} from '../reducers/userContactsSlice';

export const fetchUserContacts = createAsyncThunk(
  'userContacts/fetchUserContacts',
  async (_, thunkApi): Promise<Array<IUserContact>> => {
    try {
      return await fetchData<IUserContact>('info');
    } catch (e) {
      thunkApi.rejectWithValue('Can not load my contacts');
      return Promise.reject();
    }
  }
);
