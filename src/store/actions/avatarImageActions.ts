import {createAsyncThunk} from '@reduxjs/toolkit';
import firebase from 'firebase/compat';
import {IAvatarImage} from '../reducers/avatarImageSlice';

export const fetchAvatarImage = createAsyncThunk(
  'avatarImage/fetchAvatarImage',
  async (_, thunkApi): Promise<IAvatarImage> => {
    try {
      const db = firebase.database();
      return (await db.ref('avatarImage').once('value')).val() || {};
    } catch (e) {
      thunkApi.rejectWithValue('Can not load avatar photo');
      return Promise.reject();
    }
  }
);
