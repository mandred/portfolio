import {createAsyncThunk} from '@reduxjs/toolkit';
import {fetchData} from '../utils';
import {IProject} from '../reducers/projectsSlice';

export const fetchProjects = createAsyncThunk(
  'projects/fetchProjects',
  async (_, thunkApi): Promise<Array<IProject>> => {
    try {
      return await fetchData<IProject>('projects');
    } catch (e) {
      thunkApi.rejectWithValue('Can not load projects');
      return Promise.reject();
    }
  }
);
