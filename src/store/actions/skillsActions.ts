import {createAsyncThunk} from '@reduxjs/toolkit';
import {fetchData} from '../utils';
import {ISkill} from '../reducers/skillsSlice';

export const fetchSkills = createAsyncThunk(
  'skills/fetchSkills',
  async (_, thunkApi): Promise<Array<ISkill>> => {
    try {
      return await fetchData<ISkill>('skills');
    } catch (e) {
      thunkApi.rejectWithValue('Can not load skills');
      return Promise.reject();
    }
  }
);
