import Bowser, {Parser} from 'bowser';
import {deviceInfoSlice} from '../reducers/deviceInfoSlice';
import {
  IBrowserInfo,
  IPlatformInfo,
  ISystemInfo,
  IWindowInfo,
  WindowWidthRestrictionsEnum
} from '../reducers/deviceInfoSlice/types';
import {AppDispatch, store} from '../index';

const updateBowser = (): Parser.Parser => {
  let {userAgent} = window.navigator;

  // TODO remove after fix isses 477 in bowser - https://github.com/lancedikson/bowser/issues/477
  if (userAgent.indexOf(' (Chromium GOST)')) {
    userAgent = userAgent.replace(' (Chromium GOST)', '');
  }

  const bowser = Bowser.getParser(userAgent);

  return bowser;
};

export const updateInfo = () => (dispatch: AppDispatch): void => {
  dispatch(updatePlatformInfo());
  dispatch(updateSystemInfo());
  dispatch(updateBrowserInfo());
  dispatch(updateWindowInfo(window));
  dispatch(enableWindowSizeWatcher(window));
};

const updatePlatformInfo = () => (dispatch: AppDispatch): void => {
  const bowser = updateBowser();
  const platform = bowser.getPlatform();

  const info: IPlatformInfo = {
    type: platform.type,
    vendor: platform.vendor,
    model: platform.model,
    desktop: platform.type === 'desktop',
    tablet: platform.type === 'tablet',
    mobile: platform.type === 'mobile',
    iPhone: platform.model === 'iPhone',
    iPad: platform.model === 'iPad',
    touch: ('ontouchstart' in window) || !!window.navigator.maxTouchPoints || !!window.navigator.maxTouchPoints,
  };

  dispatch(deviceInfoSlice.actions.setPlatformInfo(info));
};

const updateSystemInfo = () => (dispatch: AppDispatch): void => {
  const bowser = updateBowser();
  const system = bowser.getOS();

  const info: ISystemInfo = {
    id: system.name?.toLowerCase(),
    name: system.name,
    version: system.version,
    versionName: system.versionName,
    majorVersion: (() => {
      let { version } = system;
      if (bowser.getOSName() === 'Windows') {
        version = version?.replace('NT ', '');
      }
      return version ? Number(version.split('.')[0]) : null;
    })(),
    windows: bowser.getOSName() === 'Windows',
    linux: bowser.getOSName() === 'Linux',
    mac: bowser.getOSName() === 'MacOS' && !('ontouchstart' in window),
    ios: bowser.getOSName() === 'iOS' || (bowser.getOSName() === 'MacOS' && 'ontouchstart' in window),
    android: bowser.getOSName() === 'Android',
    screenResolution: {
      value: `${window.screen.width * window.devicePixelRatio}x${window.screen.height * window.devicePixelRatio}`,
      width: window.screen.width * window.devicePixelRatio,
      height: window.screen.height * window.devicePixelRatio,
    }
  };

  dispatch(deviceInfoSlice.actions.setSystemInfo(info));
};

const updateBrowserInfo = () => (dispatch: AppDispatch): void => {
  const bowser = updateBowser();
  const browser = bowser.getBrowser();

  const info: IBrowserInfo = {
    id: {
      'Android Browser': 'android',
      'Google Search': 'google',
      'Microsoft Edge': 'edge',
      'Internet Explorer': 'ie',
      'Yandex Browser': 'yandex',
      'Samsung Internet for Android': 'samsung',
    }[browser.name || ''] || browser.name?.toLowerCase(),
    name: browser.name,
    vendor: window.navigator.vendor,
    version: browser.version,
    majorVersion: browser.version ? Number(browser.version.split('.')[0]) : null,
    userAgent: bowser.getUA(),
    chrome: bowser.isBrowser('Chrome'),
    firefox: bowser.isBrowser('Firefox'),
    safari: bowser.isBrowser('Safari'),
    yandex: bowser.isBrowser('Yandex Browser'),
    opera: bowser.isBrowser('Opera'),
    edge: bowser.isBrowser('Microsoft Edge'),
    ie: bowser.isBrowser('Internet Explorer'),
    samsung: bowser.isBrowser('Samsung Internet for Android'),
    language: {
      'zh-TW': 'zht',
    }[window.navigator.language] || window.navigator.language.substr(0, 2),
    locale: (() => {
      const lang = window.navigator.language;
      if (lang.length > 2 && lang.indexOf('-') !== -1) {
        return lang;
      }
      return `${lang}-${lang.toUpperCase()}`;
    })(),
    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
    cookieEnabled: window.navigator.cookieEnabled,
    indexedDbEnabled: !!window.indexedDB,
    localStorageEnabled: !!window.localStorage,
  };

  dispatch(deviceInfoSlice.actions.setBrowserInfo(info));
};

const updateWindowInfo = (payload: Window) => (dispatch: AppDispatch): void => {
  // TODO Добавить обработку в payload размера переданного элемента, а не только окна
  const width = payload.innerWidth; // TODO const width = payload.clientWidth || ....
  const height = payload.innerHeight; // TODO const height = payload.clientHeight || ....

  const info: IWindowInfo = {
    width,
    height,
    desktop: width >= WindowWidthRestrictionsEnum.DESKTOP,
    tablet: width >= WindowWidthRestrictionsEnum.TABLET && width < WindowWidthRestrictionsEnum.DESKTOP,
    mobile: width >= 0 && width < WindowWidthRestrictionsEnum.TABLET,
  };

  dispatch(deviceInfoSlice.actions.setWindowInfo(info));
};

const enableWindowSizeWatcher = (window: Window) => (dispatch: AppDispatch): void => {
  if (!store.getState().deviceInfoSlice.windowSizeWatcherEnabled) {
    // TODO добавить debounce на обновление window size
    window.addEventListener('resize', () => dispatch(updateWindowInfo(window)), false);
    dispatch(deviceInfoSlice.actions.setWindowSizeWatcherEnabled(true));
  }
};
