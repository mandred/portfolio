import {createAsyncThunk} from '@reduxjs/toolkit';
import firebase from 'firebase/compat';
import {IMainInfo} from '../reducers/mainInfoSlice';

export const fetchMainInfo = createAsyncThunk(
  'mainInfo/fetchMainInfo',
  async (_, thunkApi): Promise<IMainInfo> => {
    try {
      const db = firebase.database();
      return (await db.ref('bio').once('value')).val() || {};
    } catch (e) {
      thunkApi.rejectWithValue('Can not load info');
      return Promise.reject();
    }
  }
);
