import {createAsyncThunk} from '@reduxjs/toolkit';
import {themeTypeEnum} from '../../theme/types';

export const toggleTheme = createAsyncThunk(
  'theme/toggleTheme',
  async (_, thunkApi): Promise<string> => {
    try {
      const currentTheme: string = localStorage.getItem('appTheme') as themeTypeEnum || themeTypeEnum.LIGHT;

      localStorage.setItem('appTheme', currentTheme === themeTypeEnum.LIGHT ? themeTypeEnum.DARK : themeTypeEnum.LIGHT);

      return Promise.resolve(localStorage.getItem('appTheme') as themeTypeEnum);
    } catch (e) {
      thunkApi.rejectWithValue('Can not toggle current theme');
      return Promise.reject();
    }
  }
);
