import {createAsyncThunk} from '@reduxjs/toolkit';
import firebase from 'firebase/compat';
import {IMainImage} from '../reducers/mainImageSlice';

export const fetchMainImage = createAsyncThunk(
  'mainImage/fetchMainImage',
  async (_, thunkApi): Promise<IMainImage> => {
    try {
      const db = firebase.database();
      return (await db.ref('mainImage').once('value')).val() || {};
    } catch (e) {
      thunkApi.rejectWithValue('Can not load main photo');
      return Promise.reject();
    }
  }
);
