import firebase from 'firebase/compat';

export const fetchData = async<T> (path: string): Promise<Array<T>> => {
  const db = firebase.database();
  const response = (await db.ref(path).once('value')).val() || [];
  return Object.keys(response).map((key) => ({...response[key], id: key}));
};
