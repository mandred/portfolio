import {FC} from 'react';
import Home from '../screens/Home';
import About from '../screens/About';
import Projects from '../screens/Projects';
import Skills from '../screens/Skills';
import Contacts from '../screens/Contacts';

export interface IRoute {
  name: string,
  pathname: string,
  Component: FC,
}

export const ROUTES: Array<IRoute> = [
  {name: 'Home', pathname: '/', Component: Home},
  {name: 'About', pathname: '/about', Component: About},
  {name: 'Projects', pathname: '/projects', Component: Projects},
  {name: 'Skills', pathname: '/skills', Component: Skills},
  {name: 'Contacts', pathname: '/contacts', Component: Contacts},
];
