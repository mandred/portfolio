import React, {FC, ReactElement} from 'react';
import {useLocation} from 'react-router-dom';
import Header from '../components/Header';
import Navigation from '../components/Navigation';
import {ROUTES} from './routes';
import RouterSwitch from './RouterSwitch';
import {createUseStyles} from 'react-jss';
import {ITheme} from '../theme/types';
import {breakpointMixin, flexLayoutMixin, sizeMixin} from '../theme/styleMixins';
import {WindowWidthRestrictionsEnum} from '../store/reducers/deviceInfoSlice/types';

const useStyles = createUseStyles((theme: ITheme) => ({
  rootWrapper: {
    ...flexLayoutMixin('flex-start', 'space-between'),
    ...sizeMixin('100%'),
    flexFlow: 'column nowrap',
    backgroundColor: theme.palette.background,
    // padding: theme.spacing(1),
    overflow: 'hidden',

    ...breakpointMixin(420, 0, {
      // padding: theme.spacing(2),
    }),
    ...breakpointMixin(WindowWidthRestrictionsEnum.TABLET, 0, {
      flexFlow: 'row nowrap',
    }),
  },

  contentWrapper: {
    ...flexLayoutMixin('center', 'space-between'),
    ...sizeMixin('100%'),
    flexFlow: 'column nowrap',
    overflow: 'hidden',
  },
}));

const Router: FC = (): ReactElement => {
  const classes = useStyles();
  const location = useLocation();

  return (
    <section className={classes.rootWrapper}>
      <section className={classes.contentWrapper}>
        {ROUTES.some(({pathname}) => pathname === location.pathname) && <Header />}

        <RouterSwitch />
      </section>

      {ROUTES.some(({pathname}) => pathname === location.pathname) && <Navigation />}
    </section>
  );
};

export default Router;
