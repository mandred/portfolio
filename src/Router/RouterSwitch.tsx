import React, {FC, ReactElement} from 'react';
import {CSSTransition, SwitchTransition} from 'react-transition-group';
import {Route, Switch, useLocation} from 'react-router-dom';
import './RouterSwitch.css';
import NoMatch from '../screens/NoMatch';
import {ROUTES} from './routes';

const RouterSwitch: FC = (): ReactElement => {
  const location = useLocation();

  return (
    <SwitchTransition mode={'out-in'}>
      <CSSTransition
        key={location.pathname}
        classNames={'fade'}
        timeout={400}>
        <Switch location={location}>
          {ROUTES
            .map(({pathname, Component}) => <Route path={pathname} exact component={Component} key={pathname}/>)}

          <Route path={'*'} component={NoMatch}/>
        </Switch>
      </CSSTransition>
    </SwitchTransition>
  );
};

export default RouterSwitch;
